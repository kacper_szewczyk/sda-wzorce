package solid;

import com.sun.org.apache.xpath.internal.operations.Or;

public class ProceedOrderController {

    public void moveToRealisation (Order order) {
        if (OrderStatus.READY_TO_PREPARE.equals(order.getOrderStatus())) {
            order.setOrderStatus(OrderStatus.WAITING_FOR_PAYMENT);
        }
    }

    public void payForOrder (Order order) {
        if (OrderStatus.WAITING_FOR_PAYMENT.equals(order.getOrderStatus())) {
            order.setOrderStatus(OrderStatus.PAYED);
        }
    }
}
