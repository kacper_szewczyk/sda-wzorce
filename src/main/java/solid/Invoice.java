package solid;

import java.util.List;

public class Invoice {

    private List<OrderElement> productList;
    private String clientDetails;
    private String sellerDetails;
    private String invoiceId;

    public List<OrderElement> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderElement> productList) {
        this.productList = productList;
    }

    public String getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(String clientDetails) {
        this.clientDetails = clientDetails;
    }

    public String getSellerDetails() {
        return sellerDetails;
    }

    public void setSellerDetails(String sellerDetails) {
        this.sellerDetails = sellerDetails;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }
}
