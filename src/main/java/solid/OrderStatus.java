package solid;

public enum OrderStatus {
    READY_TO_PREPARE, WAITING_FOR_PAYMENT, PAYED, PROCEEDING, SENT, FINISHED
}
