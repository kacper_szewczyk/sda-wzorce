package solid;

import java.util.Date;

public class CreateOrderController {

    private Order order;

    public CreateOrderController() {
    }

    public void addOrderDate() {
        this.order.setOrderDate(new Date());
    }

    public void addDeadline() {
        this.order.setDeadline(new Date());
    }

    public void addProductToOrder(Product product, Integer quantity) {
        this.order.getProductList().add(new OrderElement(product, quantity));
    }

    public void setOrderNumber() {
        this.order.setId(1);
    }

    public void startOrder() {
        this.order = new Order();
    }

    public void finishOrder() {
        System.out.println(this.order.toString());
        this.order = null;
    }
}
