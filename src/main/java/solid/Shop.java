package solid;

public class Shop {

    public static void main(String[] args) {
        CreateOrderController createOrderController = new CreateOrderController();
        createOrderController.startOrder();
        createOrderController.addOrderDate();
        createOrderController.addDeadline();
        createOrderController.addProductToOrder(new Product(), 2);
        createOrderController.finishOrder();
    }
}
