package solid;

import sun.plugin.perf.PluginRollup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private Integer id;
    private Date orderDate;
    private Date deadline;
    private String clientDetails;
    private List<OrderElement> productList;
    private OrderStatus orderStatus;

    public Order() {
        productList = new ArrayList<OrderElement>();
        orderStatus = OrderStatus.READY_TO_PREPARE;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(String clientDetails) {
        this.clientDetails = clientDetails;
    }

    public List<OrderElement> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderElement> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderDate=" + orderDate +
                ", deadline=" + deadline +
                ", clientDetails='" + clientDetails + '\'' +
                ", productList=" + productList +
                '}';
    }
}
