package solid;

public class Product {
    private Integer id;
    private String sku;
    private String name;
    private String producerName;
    private Double priceNetto;
    private Double vatTax;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public Double getPriceNetto() {
        return priceNetto;
    }

    public void setPriceNetto(Double priceNetto) {
        this.priceNetto = priceNetto;
    }

    public Double getVatTax() {
        return vatTax;
    }

    public void setVatTax(Double vatTax) {
        this.vatTax = vatTax;
    }

    @Override
    public String toString() {
        return "Product{" +
                "sku='" + sku + '\'' +
                ", name='" + name + '\'' +
                ", producerName='" + producerName + '\'' +
                ", priceNetto=" + priceNetto +
                ", vatTax=" + vatTax +
                '}';
    }
}
