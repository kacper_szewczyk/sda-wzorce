import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.Scanner;

public class CallCenter {

    public static void main(String[] args) {
        System.out.println("Witaj w obsłudze klienta");
        Boolean isVerified = verifyClient();

        if (isVerified) {
            do {

            }while(proceedClient());
        }

    }

    private static Boolean proceedClient() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj typ sprawy:");
        System.out.println("1 - umówienie wizyty");
        System.out.println("2 - odwołanie wizyty");
        System.out.println("3 - sprawdzenie wyników");
        System.out.println("4 - sprawdzenie dostępnych miejsc");
        int caseType = sc.nextInt();
        return handleCaseByTypeAndCheckIfNeedToRepeat(sc, caseType);
    }

    private static Boolean verifyClient() {
        System.out.println("Podaj PESEL");
        Scanner sc = new Scanner(System.in);
        String pesel = sc.next();
        System.out.println("Podaj nazwisko panieńskie Matki");
        String motherName = sc.next();
        return true;
    }

    private static Boolean handleCaseByTypeAndCheckIfNeedToRepeat(Scanner sc, int caseType) {
        switch (caseType) {
            case 1:
                System.out.println("Podaj termin");
                String dayOfWeek = sc.next();
                break;
            case 2:
                System.out.println("Którą wizytę chcesz odwołać");
                System.out.println("1 - wtorkowa");
                System.out.println("2 - czwartkowa");
                int visitToCancel = sc.nextInt();
                break;
            case 3:
                System.out.println("Wyniki: 111");
                break;
            default:
                return true;

        }
        return false;
    }
}
