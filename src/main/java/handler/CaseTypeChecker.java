package handler;

import java.util.Scanner;

public class CaseTypeChecker extends MiddlewareChecker {

    public CaseTypeChecker() {

    }

    public Boolean check(int answerId) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj typ sprawy:");
        System.out.println("1 - umówienie wizyty");
        System.out.println("2 - odwołanie wizyty");
        System.out.println("3 - sprawdzenie wyników");
        System.out.println("4 - sprawdzenie dostępnych miejsc");
        answerId = sc.nextInt();
        return (answerId > 0 && answerId < 5) && checkNext(answerId);
    }
}
