package handler;

import java.util.Scanner;

public class CancelVisitChecker extends MiddlewareChecker {
    public static final int CASE_TYPE = 2;
    public CancelVisitChecker() {

    }

    @Override
    public Boolean check(int answerId) {
        if (answerId == CASE_TYPE) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Którą wizytę chcesz odwołać");
            System.out.println("1 - wtorkowa");
            System.out.println("2 - czwartkowa");
            int visitToCancel = sc.nextInt();
            while (visitToCancel > 2 || visitToCancel < 1) {
                System.out.println("Nie ma takiego terminu, wybierz inny:");
                visitToCancel = sc.nextInt();
            }
        }
        return checkNext(answerId);
    }
}
