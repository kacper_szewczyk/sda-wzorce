package handler;

import sun.tools.jstat.Scale;

import java.util.Scanner;

public class NeedAnotherHelpChecker extends MiddlewareChecker {

    public NeedAnotherHelpChecker() {

    }

    public Boolean check(int answerId) {
        System.out.println("Czy potrzebujesz jeszcze jakiejś pomocy?");
        System.out.println("1 - tak, 2 - nie");
        Scanner sc = new Scanner(System.in);
        switch (sc.nextInt()) {
            case 1:
                return true;
            default:
                return false;

        }
    }
}
