package handler;

public abstract class MiddlewareChecker {
    private MiddlewareChecker next;

    public MiddlewareChecker linkWith(MiddlewareChecker next) {
        this.next = next;
        return next;
    }

    public abstract Boolean check(int answerId);

    protected Boolean checkNext(int answerId) {
        if (next == null) {
            return true;
        }
        return next.check(answerId);
    }

}
