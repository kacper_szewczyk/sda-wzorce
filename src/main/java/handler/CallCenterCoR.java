package handler;

public class CallCenterCoR {

    public static void main(String[] args) {
        System.out.println("Witaj w obsłudze klienta");

        MiddlewareChecker identityVerifier = new IdentityVerifier();

        MiddlewareChecker proceedClient = new CaseTypeChecker();
        proceedClient.linkWith(new ChooseVisitDate())
                .linkWith(new CancelVisitChecker())
                .linkWith(new NeedAnotherHelpChecker());

        if (identityVerifier.check(0)) {
            while (proceedClient.check(0));
        }
    }
}
